// -------------------------------------------
// gMini : a minimal OpenGL/GLUT application
// for 3D graphics.
// Copyright (C) 2006-2008 Tamy Boubekeur
// All rights reserved.
// -------------------------------------------

// -------------------------------------------
// Disclaimer: this code is dirty in the
// meaning that there is no attention paid to
// proper class attribute access, memory
// management or optimisation of any kind. It
// is designed for quick-and-dirty testing
// purpose.
// -------------------------------------------

#include <thread>

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include "src/Vec3.h"
#include "src/Camera.h"
#include "src/Scene.h"
#include <GL/glut.h>

#include "src/matrixUtilities.h"

using namespace std;

#include "src/imageLoader.h"

#include "src/Material.h"
#ifndef COLOR_H
#include "src/Color.h"
#endif
// -------------------------------------------
// OpenGL/GLUT application code.
// -------------------------------------------

static GLint window;

static unsigned int SCREENWIDTH = 480;
static unsigned int SCREENHEIGHT = 480;

static unsigned int THREADCOUNT = 100; //PTHREAD_THREADS_MAX
static bool USETHREADS = true;

static Camera camera;
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX=0, lastY=0, lastZoom=0;
static unsigned int FPS = 0;
static bool fullScreen = false;

std::vector<Scene> scenes;
unsigned int selected_scene;

std::vector< std::pair< Vec3 , Vec3 > > rays;

//-------------------------------------------
//-------------------------------------------
void printUsage () {
    cerr << endl
         << "gMini: a minimal OpenGL/GLUT application" << endl
         << "for 3D graphics." << endl
         << "Author : Tamy Boubekeur (http://www.labri.fr/~boubek)" << endl << endl
         << "Usage : ./gmini [<file.off>]" << endl
         << "Keyboard commands" << endl
         << "------------------" << endl
         << " ?: Print help" << endl
         << " w: Toggle Wireframe Mode" << endl
         << " g: Toggle Gouraud Shading Mode" << endl
         << " f: Toggle full screen mode" << endl
         << " <drag>+<left button>: rotate model" << endl
         << " <drag>+<right button>: move model" << endl
         << " <drag>+<middle button>: zoom" << endl
         << " q, <esc>: Quit" << endl << endl;
}
void usage () {
    printUsage ();
    exit (EXIT_FAILURE);
}
void initLight () {
    GLfloat light_position[4] = {0.0, 1.5, 0.0, 1.0};
    GLfloat color[4] = { 1.0, 1.0, 1.0, 1.0};
    GLfloat ambient[4] = { 1.0, 1.0, 1.0, 1.0};

    glLightfv (GL_LIGHT1, GL_POSITION, light_position);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, color);
    glLightfv (GL_LIGHT1, GL_SPECULAR, color);
    glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambient);
    glEnable (GL_LIGHT1);
    glEnable (GL_LIGHTING);
}
void init () {
    camera.resize (SCREENWIDTH, SCREENHEIGHT);
    initLight ();
    //glCullFace (GL_BACK);
    glDisable (GL_CULL_FACE);
    glDepthFunc (GL_LESS);
    glEnable (GL_DEPTH_TEST);
    glClearColor (0.2f, 0.2f, 0.3f, 1.0f);
}
//-------------------------------------------
//-------------------------------------------

// ------------------------------------
// Replace the code of this 
// functions for cleaning memory, 
// closing sockets, etc.
// ------------------------------------
void clear () {

}

// ------------------------------------
// Replace the code of this 
// functions for alternative rendering.
// -----------------------------------

//-------------------------------------------
//-------------------------------------------
void draw () {
    glEnable(GL_LIGHTING);
    scenes[selected_scene].draw();

    // draw rays : (for debug)
    //  std::cout << rays.size() << std::endl;
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glLineWidth(6);
    glColor3f(1,0,0);
    glBegin(GL_LINES);
    for( unsigned int r = 0 ; r < rays.size() ; ++r ) {
        glVertex3f( rays[r].first[0],rays[r].first[1],rays[r].first[2] );
        glVertex3f( rays[r].second[0], rays[r].second[1], rays[r].second[2] );
    }
    glEnd();
}

void display () {
    glLoadIdentity ();
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera.apply ();
    draw ();
    glFlush ();
    glutSwapBuffers ();
}

void idle () {
    static float lastTime = glutGet ((GLenum)GLUT_ELAPSED_TIME);
    static unsigned int counter = 0;
    counter++;
    float currentTime = glutGet ((GLenum)GLUT_ELAPSED_TIME);
    if (currentTime - lastTime >= 1000.0f) {
        FPS = counter;
        counter = 0;
        static char winTitle [64];
        sprintf (winTitle, "Raytracer - FPS: %d", FPS);
        glutSetWindowTitle (winTitle);
        lastTime = currentTime;
    }
    glutPostRedisplay ();
}

//-------------------------------------------
//-------------------------------------------
int imageWidth,imageHeight;
std::vector< PixelData > image;

void postProcessBlur()
{
    int blurSize = scenes[selected_scene].lightOptions.maxBlurSize;

    Vec3* blured = new Vec3[image.size()];

    std::cout << std::endl << "   >> Blur " << blurSize << std::endl;

    for(int x = 0; x < imageWidth;x++)
    {
        for(int y = 0; y < imageHeight; y++)
        {
            int i = x + (y*imageWidth);

            float distance = abs(image[i].distance - scenes[selected_scene].lightOptions.focalDistance); //Distance from the focalDistance
            distance = std::min(1.0f,distance/scenes[selected_scene].lightOptions.focalRange);
            int blur = (int)(blurSize * distance);

            float count = 0; Vec3 color = Vec3(0,0,0);
            for(int u = std::max(0,x-blur); u <= std::min(imageWidth-1,x+blur);u++)
            {
                for(int v = std::max(0,y-blur); v <= std::min(imageHeight-1,y+blur);v++)
                {
                    float d = abs(u-x) + abs(v-y);
                    d = 1.0 / (1.0 + d);

                    count += d;
                    color += image[u + (v*imageWidth)].color * d;
                }
            }
            color /= count;
            blured[i] = color;
        }
    }

    for(int i = 0; i < image.size();i++)
    {
        image[i].color = blured[i];
    }
}

void postProcess()
{
    float max = 0;

    std::cout << std::endl << "   >> HSL" << std::endl;

    for(int i = 0; i < image.size();i++)
    {
        max = std::max(max , std::max(std::max(image[i].color[0],image[i].color[1]),image[i].color[2]));
    }

    for(int i = 0; i < image.size();i++)
    {
        if(max > 1)
        {
            image[i].color[0] /= max;
            image[i].color[1] /= max;
            image[i].color[2] /= max;
        }

        Color c = Color(image[i].color);
        c.computeHSL();
        c.shift(scenes[selected_scene].lightOptions.hueShift);
        c.saturate(scenes[selected_scene].lightOptions.saturation);
        c.enlight(scenes[selected_scene].lightOptions.luminosity);
        c.computeRGB();
        image[i].color = c.getRGB();
    }

    postProcessBlur();
}

void ray_trace_method(Vec3 pos, Vec3 dir, int nsamples,int x, int y, float u, float v, int w,int h)// int h,int nsamples)
{
    Vec3 color = Vec3(0,0,0); float distance = 0;
    for( unsigned int s = 0 ; s < nsamples ; ++s )
    {
        /*float ran = rand()/RAND_MAX;

        float u = ((float)(x) + ran) / w;
        float v = ((float)(y) + ran) / h;

        // this is a random uv that belongs to the pixel xy.
        screen_space_to_world_space_ray(u,v,pos,dir);*/

        PixelData output = scenes[selected_scene].rayTrace( Ray(pos , dir), camera );

        color += output.color;
        distance += output.distance;
    }

    image[x + y*w].color = color / (float)nsamples;
    image[x + y*w].distance = distance / (float)nsamples;
}

void ray_trace_from_camera() {
    unsigned int nsamples = scenes[selected_scene].lightOptions.sampleCount;

    int w = glutGet(GLUT_WINDOW_WIDTH)  ,   h = glutGet(GLUT_WINDOW_HEIGHT);
    std::cout << "Ray tracing a " << w << " x " << h << " image  with " << nsamples << " samples " << (USETHREADS ? "using threads" : "")   << std::endl;
    camera.apply();

    thread threads[THREADCOUNT];

    int size = w*h;
    Vec3 pos , dir;
    image.resize( size , PixelData(Vec3(0,0,0),0) );
    int lastProgress = 0;

    for (int y=0; y<h; y++)
    {
        for (int x=0; x<w; x++)
        {
            float u = ((float)(x)) / w;
            float v = ((float)(y)) / h;

            // this is a random uv that belongs to the pixel xy.
            screen_space_to_world_space_ray(u,v,pos,dir);

            if(USETHREADS){
                int i = x + (y*w);
                int threadId = i%THREADCOUNT;

                if(i >= THREADCOUNT){threads[threadId].join();}

                threads[threadId] = thread(ray_trace_method,pos,dir,nsamples,x,y,u,v,w,h);//,h,nsamples);
            }
            else{
                ray_trace_method(pos,dir,nsamples,x,y,u,v,w,h);//,h,nsamples);
            }
        }

        int progress = (int)((y/(float)(h))*100);
        if(progress != lastProgress){
            std::cout << '\r' << "\t" << progress << "%" << std::flush;
            lastProgress = progress;
        }
    }
    if(USETHREADS){
        for(int i = 0; i < THREADCOUNT;i++)
        {
            threads[i].join();
        }
    }

    imageWidth = w; imageHeight = h;

    std::cout << std::endl << "Post-processing " << imageWidth << "x" << imageHeight << "..." << std::endl;

    postProcess();

    std::cout << std::endl << "Render finished" << std::endl;

    std::string filename = "./rendu.ppm";
    ofstream f(filename.c_str(), ios::binary);
    if (f.fail()) {
        cout << "Could not open file: " << filename << endl;
        return;
    }
    f << "P3" << std::endl << w << " " << h << std::endl << 255 << std::endl;
    for (int i=0; i<w*h; i++)
        f << (int)(255.f*std::min<float>(1.f,image[i].color[0])) << " " << (int)(255.f*std::min<float>(1.f,image[i].color[1])) << " " << (int)(255.f*std::min<float>(1.f,image[i].color[2])) << " ";
    f << std::endl;
    f.close();

    std::cout << "Image saved" << std::endl;
}
//-------------------------------------------
//-------------------------------------------

void key (unsigned char keyPressed, int x, int y) {
    Vec3 pos , dir;
    switch (keyPressed) {
    case 'f':
        if (fullScreen == true) {
            glutReshapeWindow (SCREENWIDTH, SCREENHEIGHT);
            fullScreen = false;
        } else {
            glutFullScreen ();
            fullScreen = true;
        }
        break;
    case 'q':
    case 27:
        clear ();
        exit (0);
        break;
    case 'w':
        GLint polygonMode[2];
        glGetIntegerv(GL_POLYGON_MODE, polygonMode);
        if(polygonMode[0] != GL_FILL)
            glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
        else
            glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
        break;

    case 'r':
    	// char* c;
    	// std::cout << "Enter sample count : ";
    	// std::cin >> c; sampleCount = atoi(c);
    	// std::cout << "  |  " << c << std::endl; 
        camera.apply();
        rays.clear();
        ray_trace_from_camera();
        break;
    case '+':
        selected_scene++;
        if( selected_scene >= scenes.size() ) selected_scene = 0;
        break;
    case '8':
        camera.zoom(0.1);break;
    case '2':
        camera.zoom(-0.1);break;
    default:
        printUsage ();
        break;
    }
    idle ();
}

void mouse (int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        mouseMovePressed = false;
        mouseRotatePressed = false;
        mouseZoomPressed = false;
    } else {
        if (button == GLUT_LEFT_BUTTON) {
            camera.beginRotate (x, y);
            mouseMovePressed = false;
            mouseRotatePressed = true;
            mouseZoomPressed = false;
        } else if (button == GLUT_RIGHT_BUTTON) {
            lastX = x;
            lastY = y;
            mouseMovePressed = true;
            mouseRotatePressed = false;
            mouseZoomPressed = false;
        } else if (button == GLUT_MIDDLE_BUTTON) {
            if (mouseZoomPressed == false) {
                lastZoom = y;
                mouseMovePressed = false;
                mouseRotatePressed = false;
                mouseZoomPressed = true;
            }
        }
    }
    idle ();
}

void motion (int x, int y) {
    if (mouseRotatePressed == true) {
        camera.rotate (x, y);
    }
    else if (mouseMovePressed == true) {
        camera.move ((x-lastX)/static_cast<float>(SCREENWIDTH), (lastY-y)/static_cast<float>(SCREENHEIGHT), 0.0);
        lastX = x;
        lastY = y;
    }
    else if (mouseZoomPressed == true) {
        camera.zoom (float (y-lastZoom)/SCREENHEIGHT);
        lastZoom = y;
    }
}


void reshape(int w, int h) {
    camera.resize (w, h);
}

//-------------------------------------------
//-------------------------------------------

int main (int argc, char ** argv) {
    if (argc > 2) {
        printUsage ();
        exit (EXIT_FAILURE);
    }

    if(argc >= 2)
    {
        //scenes[selected_scene].lightOptions.sampleCount = std::atoi(argv[1]);
        USETHREADS = atoi(argv[1]) == 1;
    }

    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize (SCREENWIDTH, SCREENHEIGHT);
    window = glutCreateWindow ("gMini");

    init ();
    glutIdleFunc (idle);
    glutDisplayFunc (display);
    glutKeyboardFunc (key);
    glutReshapeFunc (reshape);
    glutMotionFunc (motion);
    glutMouseFunc (mouse);
    key ('?', 0, 0);

    camera.move(0., 0., -3.1);
    selected_scene=2;
    scenes.resize(3);
    scenes[0].setup_single_sphere();
    scenes[1].setup_single_square();
    scenes[2].setup_cornell_box();

    glutMainLoop ();
    return EXIT_SUCCESS;
}

