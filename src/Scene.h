#ifndef SCENE_H
#define SCENE_H

#include <vector>
#include <string>
#include "Mesh.h"
#include "Sphere.h"
#include "Square.h"
#include "Camera.h"
#include <cstdio>
#include <cstdlib>

#include <GL/glut.h>
#include "LightOptions.h"
#include "Color.h"

enum LightType {
    LightType_Spherical,
    LightType_Quad
};


struct PixelData
{
    Vec3 color;
    float distance;

    PixelData(Vec3 _color, float _distance)
    {
        color = _color; distance = _distance;
    }
};

struct Light {
    Vec3 material;
    bool isInCamSpace;
    LightType type;

    Vec3 pos;
    float radius;

    Mesh quad;

    float powerCorrection;

    Light() : powerCorrection(1.0) {}

};

struct RaySceneIntersection{
    bool intersectionExists;
    unsigned int typeOfIntersectedObject;
    unsigned int objectIndex;
    float t;
    float u,v;

    Vec3 point;
    Vec3 normal;
    Vec3 tangent;

    RayTriangleIntersection rayMeshIntersection;
    RaySphereIntersection raySphereIntersection;
    RaySquareIntersection raySquareIntersection;
    RaySceneIntersection() : intersectionExists(false) , t(FLT_MAX) {}
};



class Scene {
    std::vector< Mesh > meshes;
    std::vector< Sphere > spheres;
    std::vector< Square > squares;
    std::vector< Light > lights;

public:
    LightOptions lightOptions;

    Scene() {
    }

    void draw() {
        // iterer sur l'ensemble des objets, et faire leur rendu :
        for( unsigned int It = 0 ; It < meshes.size() ; ++It ) {
            Mesh const & mesh = meshes[It];
            mesh.draw();
        }
        for( unsigned int It = 0 ; It < spheres.size() ; ++It ) {
            Sphere const & sphere = spheres[It];
            sphere.draw();
        }
        for( unsigned int It = 0 ; It < squares.size() ; ++It ) {
            Square const & square = squares[It];
            square.draw();
        }
    }

    Material getMaterial(RaySceneIntersection raySceneIntersection)
    {
        if(raySceneIntersection.intersectionExists){
            if(raySceneIntersection.typeOfIntersectedObject == 0){
                return spheres[raySceneIntersection.objectIndex].material;
            }
            else if(raySceneIntersection.typeOfIntersectedObject == 1)
            {
                return squares[raySceneIntersection.objectIndex].material;
            }
            else if(raySceneIntersection.typeOfIntersectedObject == 2)
            {
                return meshes[raySceneIntersection.objectIndex].material;
            }
        }
        Material m; m.nul = true;
        return m;
    }

    RaySceneIntersection computeIntersection(Ray const & ray, float cameraNear, float cameraFar)
    {
        RaySceneIntersection result; 

        result.t = FLT_MAX;
        result.typeOfIntersectedObject = 0; //--- type is sphere
        result.intersectionExists = false;

        for( unsigned int It = 0 ; It < spheres.size() ; ++It ) 
        {
            Sphere const & sphere = spheres[It];

            RaySphereIntersection rsi = sphere.intersect(ray);

          	if(rsi.intersectionExists && rsi.t < result.t && rsi.t >= cameraNear && rsi.t <= cameraFar)
          	{
          		result.objectIndex = It; result.t = rsi.t; result.raySphereIntersection = rsi; 
                result.point = rsi.intersection; result.normal = rsi.normal; result.tangent = rsi.tangent;
                result.u = rsi.u; result.v = rsi.v;
                result.intersectionExists = true;
          	}
        }


        for( unsigned int It = 0 ; It < squares.size() ; ++It ) 
        {
            Square const & square = squares[It];

            RaySquareIntersection rsi = square.intersect(ray);

            if(rsi.intersectionExists && rsi.t < result.t && rsi.t >= cameraNear && rsi.t <= cameraFar)
            {
                result.objectIndex = It; result.t = rsi.t; result.raySquareIntersection = rsi; 
                result.typeOfIntersectedObject = 1; //----- type is square
                result.point = rsi.intersection; result.normal = rsi.normal;result.tangent = rsi.tangent;
                result.u = rsi.u; result.v = rsi.v;
                result.intersectionExists = true;
            }
        }

        for( unsigned int It = 0 ; It < meshes.size() ; ++It ) 
        {
            Mesh const & mesh = meshes[It];

            RayTriangleIntersection rsi = mesh.intersect(ray);

            if(rsi.intersectionExists && rsi.t < result.t && rsi.t >= cameraNear && rsi.t <= cameraFar)
            {
                result.objectIndex = It; result.t = rsi.t; result.rayMeshIntersection = rsi;
                result.typeOfIntersectedObject = 2; //----- type is mesh
                result.point = rsi.intersection; result.normal = rsi.normal;result.tangent = rsi.tangent;
                result.u = rsi.u; result.v = rsi.v;
                result.intersectionExists = true;
            }
        }

        result.normal.normalize();
        result.point += lightOptions.intersectionDelta * result.normal;

        return result;
    }

    //Return the ratio of light received a the ray origin's, depending of the objects on the way (that could be transparent)
    Vec3 shadowCast(Ray const & ray, float maxDist,Light light)
    {
        if(maxDist <= 0){return Vec3(1,1,1);}

        RaySceneIntersection rsi = computeIntersection(ray,0.001,maxDist);
        if(!rsi.intersectionExists){return Vec3(1,1,1);}

        Material mat = getMaterial(rsi);

        if(!mat.nul)
        {
            if(mat.type == Material_Glass)
            {
                Vec3 result = Vec3(0,0,0);              
                /*
                Color c = Color(1,0,0,0,0,0);
                c.computeHSL();

                int unit = 360 / lightOptions.prismeCount;
                float stepMedium = (mat.light_medium * mat.prismarity) - mat.light_medium;

                for(int i = 0; i < lightOptions.prismeCount;i++)
                {*/

                Color c = Color(1,0,0,0,0,0);
                c.computeHSL();

                	//float r = i / (float)lightOptions.prismeCount;
                	//float usedMedium = mat.light_medium + (stepMedium*r);
                float usedMedium = mat.light_medium;

                Vec3 refractedDir;
                if(Vec3::dot(ray.direction(),rsi.normal) <= 0)
                {
                    refractedDir = Vec3::refract(ray.direction(),rsi.normal,usedMedium);
                }
                else{
                    refractedDir = Vec3::refract(ray.direction(),(-1.0)*rsi.normal,usedMedium);
                }

                float angle = (1-abs(Vec3::dot(ray.direction(),refractedDir)));
                float hue = angle  * mat.prismarity * 3;

                c.H = ( 280 - (int)(360 * hue)) % 360;
                if(hue > 1)
                {
                	c.S = 1.0 - (hue-1);
                	c.L = hue-1;
                }
                else if(hue < 0.5)
                {
                	c.S = hue * 2;
                	c.L = hue * 2;
                }

                float maxV = pow(mat.light_medium_pow,0.5);
                float minV = 0.5;
                //std::cout << c.H << std::endl;
                angle = pow(1-angle,mat.light_medium_pow);
               	angle = minV + ((maxV-minV) * angle);

                    /*c.computeRGB();
                    result +=  angle * c.getRGB();

                    /*std::cout << i << " > medium = " << usedMedium << "  angle = " << angle
                    << "  " << (int)c.R << " " << (int)c.G << " " << (int)c.B << std::endl;//

                    c.shift(unit);
                }
                result /= lightOptions.prismeCount;*/

                if(mat.prismarity > 0){
                    c.computeRGB();
                    result = c.getRGB() * angle;//Vec3(1,1,1) * angle;
                }
                else
                {
                    result = Vec3(1,1,1) * angle;
                }

                Ray newray = Ray(rsi.point + (ray.direction() * 0.01),ray.direction());

                return result * mat.transparency * shadowCast(newray,maxDist - rsi.t,light) ;
            }
        }

        return Vec3(0,0,0);
    }

    Vec3 computeLightning(Ray ray,RaySceneIntersection rsi,Material mat)
    {
        Vec3 dColor = mat.getDiffuse(rsi.u,rsi.v);
        Vec3 sColor = mat.getSpecular(rsi.u,rsi.v);
        float metalness = mat.getMetal(rsi.u,rsi.v);
        float roughness = mat.getRough(rsi.u,rsi.v);

        float usedShininess = (mat.shininess[0]
                                + ((mat.shininess[1] - mat.shininess[0]) * metalness))
                                / (1.0+roughness);

        Vec3 diffuseColor = Vec3(0,0,0); Vec3 specularColor = Vec3(0,0,0); Vec3 unit = Vec3(1,1,1);

        Vec3 diffuseCount =  Vec3(0,0,0); Vec3 specCount =  Vec3(0,0,0); float lightCount = 0;

        Vec3 reflectedDir = Vec3::reflect(ray.direction(),rsi.normal); reflectedDir.normalize();

        for(int i = 0; i < lights.size();i++)
        {
            Light light = lights[i];

            Vec3 diffuseAmount = Vec3(0,0,0);
            Vec3 specAmount = Vec3(0,0,0);

            for(int p = 0; p < lightOptions.shadowSoftness;p++)
            {
                Vec3 pos;
                if(light.type == LightType::LightType_Spherical)
                {
                   Vec3 radius = Vec3(  ((float)rand() / (float)RAND_MAX) * lights[i].radius,
                                        ((float)rand() / (float)RAND_MAX) * lights[i].radius,
                                        ((float)rand() / (float)RAND_MAX) * lights[i].radius);

                   pos = lights[i].pos + radius;
                }
                else{

                }

                Vec3 dir = pos - rsi.point; float dist = dir.length();
                dir.normalize();
                
                float angle = Vec3::dot(dir,rsi.normal);
                angle = pow(angle,1.0 / (1.0+roughness)); //Curving the angle value

                if(angle <= 0){/*No light*/}
                else{
                	Ray ray = Ray( rsi.point,dir);
                	//Checking if their's an object in the direction towards the light

                	Vec3 shadowLightRatio = shadowCast(ray,dist,light); //computeIntersection(ray,0.001,FLT_MAX);

                	//If there is, checking if it is blocking the light
                	if(shadowLightRatio[0] <= 0 && shadowLightRatio[1] <= 0 && shadowLightRatio[2] <= 0){/*No light*/}
                	else{
                    	float distReduc = pow(1.0+dist,lightOptions.lightIntensityCurve);
              
                    	diffuseAmount += (angle * shadowLightRatio) * unit
                                            * (1.0/distReduc)
                                            * (1.0-metalness);

                		float reflect = Vec3::dot(reflectedDir,dir);
                		reflect = std::max(0.0f,reflect);
                		reflect = pow(reflect,lightOptions.reflectionCurve*usedShininess);

                    	specAmount += reflect * shadowLightRatio * unit;
                	}
                }
            }

            specAmount /= (float)(lightOptions.shadowSoftness*2);
            diffuseAmount /= (float)(lightOptions.shadowSoftness*2);

            diffuseColor += (light.material * light.powerCorrection) * diffuseAmount;
            specularColor +=  (light.material * light.powerCorrection) * specAmount;

            diffuseCount += diffuseAmount;
            specCount += specAmount;

            lightCount += light.powerCorrection;
        }

        float reductor = (lightOptions.global_light_coef/ lightCount );

        switch(lightOptions.lightMode)
        {
            case Additive:{
                Vec3 diffuseLightCoef = diffuseCount * lightOptions.lightsContribution;
                Vec3 specLightCoef = specCount * lightOptions.lightsContribution;

                Vec3 diffuseMatCoef = diffuseCount * (1-lightOptions.lightsContribution);
                Vec3 specMatCoef = specCount * (1-lightOptions.lightsContribution);

                return (lightOptions.ambient_light * dColor) +
                        ((diffuseMatCoef * reductor) * dColor) +
                        ((specMatCoef * reductor) * sColor) +
                        ((diffuseLightCoef * reductor) * diffuseColor) +
                        ((specLightCoef * reductor) * specularColor);}
            case Led:{
                dColor = Vec3(dColor[0] * diffuseColor[0],dColor[1] * diffuseColor[1],dColor[2] * diffuseColor[2]);
                sColor = Vec3(sColor[0] * specularColor[0],sColor[1] * specularColor[1],sColor[2] * specularColor[2]);
                return (lightOptions.ambient_light * dColor) +
                        (dColor * reductor) +
                        (sColor * reductor);}
        }
    }

    PixelData rayTraceRecursive( Ray ray , int NRemainingBounces, float cameraNear, float cameraFar ) {

        RaySceneIntersection raySceneIntersection = computeIntersection(ray, cameraNear, cameraFar);
        float distance = raySceneIntersection.t;

        Vec3 dColor = lightOptions.scene_color;
        Vec3 sColor;
        Material mat = getMaterial(raySceneIntersection);

        //Checking if we found an intersection and recovering the object color
        if(mat.nul)
        {
            return PixelData(lightOptions.scene_color,0);
        }

        //UV test  return PixelData(Vec3(raySceneIntersection.u,raySceneIntersection.v,0),raySceneIntersection.t);

        //=============================================================================================
        //Applying material normal
        //=============================================================================================

        Vec3 matNormal = mat.getNormal(raySceneIntersection.u,raySceneIntersection.v);
        raySceneIntersection.normal =
            (raySceneIntersection.normal * matNormal[2]) +
            (raySceneIntersection.tangent * matNormal[1]) +
            (Vec3::cross(raySceneIntersection.normal,raySceneIntersection.tangent) * matNormal[0]);

        //Texture test return diffuseColor;

        //=============================================================================================
        //Computing the object light
        //=============================================================================================

        if(mat.type == Material_Mirror)
        {
            if(NRemainingBounces>0)
            {
                Vec3 reflectedDir = Vec3::reflect(ray.direction(),raySceneIntersection.normal); reflectedDir.normalize();
                Ray newRay = Ray(raySceneIntersection.point,reflectedDir);

                Vec3 color = rayTraceRecursive(newRay,NRemainingBounces-1,0,cameraFar).color;
                color += computeLightning(ray,raySceneIntersection,mat);
                color = Vec3(mat.mirror_material[0] * color[0], mat.mirror_material[1] * color[1],mat.mirror_material[2] * color[2]);

                return PixelData(color,distance);
            }
        }
        else if(mat.type == Material_Metal)
        {
            if(NRemainingBounces>0)
            {
                Vec3 reflectedDir = Vec3::reflect(ray.direction(),raySceneIntersection.normal); reflectedDir.normalize();
                Ray newRay = Ray(raySceneIntersection.point,reflectedDir);

                Vec3 color = rayTraceRecursive(newRay,0,0,cameraFar).color * mat.getMetal(raySceneIntersection.u,raySceneIntersection.v);
                color = color * mat.getSpecular(raySceneIntersection.u,raySceneIntersection.v);

                Color hslColor = Color(color);hslColor.computeHSL();
            	hslColor.saturate(1.0/mat.getRough(raySceneIntersection.u,raySceneIntersection.v));
            	hslColor.computeRGB(); color = hslColor.getRGB();

                color += computeLightning(ray,raySceneIntersection,mat);

            	return PixelData(color,distance);
            }
        }
        else if(mat.type == Material_Glass)
        {
        	if(NRemainingBounces>0)
            {
            	Vec3 refractedDir;Vec3 point;
            	if(Vec3::dot(ray.direction(),raySceneIntersection.normal) <= 0)
            	{
            		refractedDir = Vec3::refract(ray.direction(),raySceneIntersection.normal,mat.index_medium);
            		point = raySceneIntersection.point - (raySceneIntersection.normal * 2 * lightOptions.intersectionDelta);
            	}
            	else{
            		refractedDir = Vec3::refract(ray.direction(),(-1.0)*raySceneIntersection.normal,mat.index_medium);
            		point = raySceneIntersection.point;
            	}
            	
            	Ray newRay = Ray(point,refractedDir);

            	Vec3 vcolor = rayTraceRecursive(newRay,NRemainingBounces-1,0,cameraFar).color;

            	Color hslColor = Color(vcolor);
            	hslColor.computeHSL();
            	hslColor.saturate(mat.transparency);
            	hslColor.computeRGB();

            	return PixelData(hslColor.getRGB(),distance);
            }
        }

        return PixelData(computeLightning(ray,raySceneIntersection,mat),distance);
    }


    PixelData rayTrace( Ray const & rayStart , Camera camera) {
        //TODO appeler la fonction recursive
        return  rayTraceRecursive(rayStart,lightOptions.bounceCount, camera.getNearPlane(),camera.getFarPlane());
    }

    void setup_single_sphere() {
        meshes.clear();
        spheres.clear();
        squares.clear();
        lights.clear();

        {
            lights.resize( lights.size() + 1 );
            Light & light = lights[lights.size() - 1];
            light.pos = Vec3(-5,5,5);
            light.radius = 2.5f;
            light.powerCorrection = 2.f;
            light.type = LightType_Spherical;
            light.material = Vec3(1,1,1);
            light.isInCamSpace = false;
        }
        {
            spheres.resize( spheres.size() + 1 );
            Sphere & s = spheres[spheres.size() - 1];
            s.m_center = Vec3(1.0 , 0. , 0.);
            s.m_radius = 0.5f;
            s.build_arrays();
            s.material.type = Material_Mirror;
            s.material.diffuse_material = Vec3( 1.,0.8,0.8 );
            s.material.specular_material = Vec3( 0.2,0.2,0.2 );
            //s.material.shininess = 20;
        }
    }

    void setup_single_square() {
        meshes.clear();
        spheres.clear();
        squares.clear();
        lights.clear();

        {
            lights.resize( lights.size() + 1 );
            Light & light = lights[lights.size() - 1];
            light.pos = Vec3(-5,5,5);
            light.radius = 2.5f;
            light.powerCorrection = 2.f;
            light.type = LightType_Spherical;
            light.material = Vec3(1,1,1);
            light.isInCamSpace = false;
        }

        {
            squares.resize( 3 );
            Square & s = squares[0];
            s.setQuad(Vec3(-1.5, -1.5, 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 0.8,0.8,0.8 );
            s.material.specular_material = Vec3( 0.8,0.8,0.8 );
            //s.material.shininess = 20;

            Square & s2 = squares[1];
            // s2.setQuad(Vec3(-1, -1, 0), Vec3(0,2.5, 0), Vec3(0., 0, 2.5), 2., 2.);
            // s2.build_arrays();
            // s2.material.diffuse_material = Vec3( 1,0.3,0.3 );
            // s2.material.specular_material = Vec3( 0.8,0.8,0.8 );
            // s2.material.shininess = 20;
            s2.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s2.scale(Vec3(2., 2., 1.));
            s2.translate(Vec3(0., 0., 2.));
            s2.rotate_y(90);
            s2.build_arrays();
            s2.material.diffuse_material = Vec3( 1.,0.,0. );
            s2.material.specular_material = Vec3( 1.,0.,0. );
            s2.material.shininess = Vec3(10,16,1);

            Square & s3 = squares[2];
            // s2.setQuad(Vec3(-1, -1, 0), Vec3(0,2.5, 0), Vec3(0., 0, 2.5), 2., 2.);
            // s2.build_arrays();
            // s2.material.diffuse_material = Vec3( 1,0.3,0.3 );
            // s2.material.specular_material = Vec3( 0.8,0.8,0.8 );
            // s2.material.shininess = 20;
            s3.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s3.scale(Vec3(2., 2., 1.));
            s3.translate(Vec3(0., 0., 2.));
            s3.rotate_y(-90);
            s3.build_arrays();
            s3.material.diffuse_material = Vec3( 1.,0.,0. );
            s3.material.specular_material = Vec3( 1.,0.,0. );
            s3.material.shininess = Vec3(10,16,1);

            /*Square & s1 = squares[1];
            s1.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s1.scale(Vec3(2., 2., 1.));
            s1.translate(Vec3(0., 0., -2.));
            //s1.rotate_y(90);
            s1.build_arrays();
            s1.material.diffuse_material = Vec3( 1.,0.,0. );
            s1.material.specular_material = Vec3( 1.,0.,0. );
            s1.material.shininess = Vec3(10,16,1);*/
        }
        {
            spheres.resize( spheres.size() + 1 );
            Sphere & s = spheres[spheres.size() - 1];
            s.m_center = Vec3(0.7 , 0. , 0.5);
            s.m_radius = 0.5f;
            s.build_arrays();
            s.material.type = Material_Mirror;
            s.material.diffuse_material = Vec3( 1.,0.8,0.8 );
            s.material.specular_material = Vec3( 0.2,0.2,0.2 );
            //s.material.shininess = 20;
        }
    }

    void setup_cornell_box(){
        meshes.clear();
        spheres.clear();
        squares.clear();
        lights.clear();

        {
            /*lights.resize( 1 );

            Light & light = lights[0];
            light.pos = Vec3( 0, 1.25, 0 );
            light.radius = 0.45f;
            light.powerCorrection = 1.0f;
            light.type = LightType_Spherical;
            light.material = Vec3(1,1,1);
            light.isInCamSpace = false;*/

            /*lights.resize( 3 );

            Light & light = lights[0];
            light.pos = Vec3( -1.2, 1.25, -0.7 );
            light.radius = 0.2f;
            light.powerCorrection = 1.0f;
            light.type = LightType_Spherical;
            light.material = Vec3(1,0,0);
            light.isInCamSpace = false;

            Light & light2 = lights[1];
            light2.pos = Vec3( 0, 0.75, -0.7 );
            light2.radius = 0.2f;
            light2.powerCorrection = 1.0f;
            light2.type = LightType_Spherical;
            light2.material = Vec3(0,1,0);
            light2.isInCamSpace = false;

            Light & light3 = lights[2];
            light3.pos = Vec3( 1.2, 1.25, -0.7 );
            light3.radius = 0.2f;
            light3.powerCorrection = 1.0f;
            light3.type = LightType_Spherical;
            light3.material = Vec3(0,0,1);
            light3.isInCamSpace = false;*/


            lights.resize( 1 );
            
            Light & light = lights[0];
            light.pos = Vec3( 1, 1.25, -0.7 );
            light.radius = 0.3f;
            light.powerCorrection = 4.0f;
            light.type = LightType_Spherical;
            light.material = Vec3(1,1,1);
            light.isInCamSpace = false;

            /*Light & light2 = lights[1];
            light2.pos = Vec3( -1.25, 0, -0.7 );
            light2.radius = 0.05f;
            light2.powerCorrection = 1.0f;
            light2.type = LightType_Spherical;
            light2.material = Vec3(0,0.8,1);
            light2.isInCamSpace = false;*/
        }

        { //Mesh

            meshes.push_back(Mesh("meshes/cylinder.off",
                Vec3(0, 0, -2),
                Vec3(0,90,0),
                Vec3(1,2,1)));

            /*meshes.push_back(Mesh("meshes/2triangles.off",
                Vec3(-0.5, 0.5, 0),
                Vec3(80,0,0),
                Vec3(0.5,0.5,0.5)));*/

            meshes[0].material.diffuse_material = Vec3( 0,0,0 );
            meshes[0].material.specular_material = Vec3( 1,1,1 );
            meshes[0].material.shininess = Vec3(10,16,1);
            meshes[0].material.type = Material_Mirror;
            meshes[0].material.mirror_material = Vec3(0.8,0.8,0.8);

            /*meshes.push_back(Mesh("meshes/iko.off",
                Vec3(1, -1.3, 0.5),
                Vec3(0,0,0),
                Vec3(0.7,0.7,0.7)));

            meshes[1].material.diffuse_material = Vec3( 0,0,0 );
            meshes[1].material.specular_material = Vec3( 1,1,1 );
            meshes[1].material.shininess = Vec3(10,16,1);
            meshes[1].material.type = Material_Glass;
            meshes[1].material.mirror_material = Vec3(0.8,0.8,0.8);

            meshes[1].material.transparency = 0.7;
            meshes[1].material.index_medium = 0.92;
            meshes[1].material.light_medium = 0.3;
            meshes[1].material.light_medium_pow = 6;
            meshes[1].material.prismarity = 2;*/
        }

        { //Back Wall
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.scale(Vec3(2., 2., 1.));
            s.translate(Vec3(0., 0., -2.));
            s.build_arrays();
            s.material.diffuse_material = Vec3( 0.7,0.7,1.0 );
            s.material.specular_material = Vec3( 0.7,0.7,1.0 );

            s.material.shininess = Vec3(3,16,1);
            s.material.metalness = 0.9f;
            s.material.roughness = 3.0f;

            s.material.type = Material_Metal;

            s.material.loadColorTexture("./textures/Rust_Color.jpg");
            s.material.loadNormalTexture("./textures/Rust_Normal.jpg");
            s.material.loadMetalTexture("./textures/Rust_Metal.jpg");
            s.material.loadRoughTexture("./textures/Rust_Rough.jpg");
        }

        { //Left Wall

            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.scale(Vec3(2., 2., 1.));
            s.translate(Vec3(0., 0., -2.));
            s.rotate_y(90);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 0.8,0.8,0.8 );
            s.material.specular_material = Vec3( 0.8,0.8,0.8 );

            s.material.shininess = Vec3(10,16,1);
            s.material.metalness = 0.9f;
            s.material.roughness = 3.0f;

            s.material.loadColorTexture("./textures/Rust2_Color.jpg");
            s.material.loadNormalTexture("./textures/Rust2_Normal.jpg");
            s.material.loadMetalTexture("./textures/Rust2_Metal.jpg");
            s.material.loadRoughTexture("./textures/Rust2_Rough.jpg");
        }

        { //Right Wall
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.translate(Vec3(0., 0., -2.));
            s.scale(Vec3(2., 2., 1.));
            s.rotate_y(-90);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 0.0,1.0,0.0 );
            s.material.specular_material = Vec3( 0.0,1.0,0.0 );
            s.material.shininess = Vec3(10,16,1);
        }

        { //Floor
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.translate(Vec3(0., 0., -2.));
            s.scale(Vec3(2., 2., 1.));
            s.rotate_x(-90);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 0.8,1.0,0.8 );
            s.material.specular_material = Vec3( 0.8,1.0,0.8 );

            s.material.shininess = Vec3(10,16,1);
            s.material.roughness = 5.0f;

            s.material.loadColorTexture("./textures/Floor_Color.jpg");
            s.material.loadNormalTexture("./textures/Floor_Normal.jpg");
            s.material.loadRoughTexture("./textures/Floor_Rough.jpg");
        }

        { //Ceiling
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.translate(Vec3(0., 0., -2.));
            s.scale(Vec3(2., 2., 1.));
            s.rotate_x(90);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 1.0,1.0,1.0 );
            s.material.specular_material = Vec3( 1.0,1.0,1.0 );

            s.material.shininess = Vec3(10,16,1);
            s.material.roughness = 5.0f;

            s.material.loadColorTexture("./textures/Ceiling_Color.jpg");
            s.material.loadNormalTexture("./textures/Ceiling_Normal.jpg");
            s.material.loadRoughTexture("./textures/Ceiling_Rough.jpg");
        }
        
        { //Front Wall

            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.translate(Vec3(0., 0., -2.));
            s.scale(Vec3(2., 2., 1.));
            s.rotate_y(180);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 0.0,0.2,1.0 );
            s.material.specular_material = Vec3( 0.0,0.2,1.0 );
            s.material.shininess = Vec3(10,16,1);
        }


        { //MIRRORED Sphere

            spheres.resize( spheres.size() + 1 );
            Sphere & s = spheres[spheres.size() - 1];
            s.m_center = Vec3(-1.0, -1.25, -0.5);//Vec3(1.0, -1.25, 0.7);
            s.m_radius = 0.75f;
            s.build_arrays();
            s.material.type = Material_Mirror;
            s.material.mirror_material = Vec3(1,0.2,0.2);
            s.material.diffuse_material = Vec3( 1.,0.,0. );
            s.material.specular_material = Vec3( 1.,0.,0. );
            s.material.shininess = Vec3(10,16,1);

            //s.material.loadColorTexture("./textures/Ceiling_Color.jpg");
        }


        { //GLASS Sphere
            spheres.resize( spheres.size() + 1 );
            Sphere & s = spheres[spheres.size() - 1];
            s.m_center = Vec3(1.0, -1.25, 0.5);//Vec3(-1.0, -1.25, -0.5);
            s.m_radius = 0.75f;
            s.build_arrays();
            s.material.type = Material_Glass;
            s.material.diffuse_material = Vec3( 1.,1.,1. );
            s.material.specular_material = Vec3(  1.,1.,1. );
            s.material.shininess = Vec3(10,16,1);

            s.material.transparency = 0.7;
            s.material.index_medium = 0.92;
            s.material.light_medium = 0.3;
            s.material.light_medium_pow = 12;
            s.material.prismarity = 0;
        }

    }

};



#endif
