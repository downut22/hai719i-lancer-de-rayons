#ifndef SQUARE_H
#define SQUARE_H
#include "Vec3.h"
#include <vector>
#include "Mesh.h"
#include <cmath>

struct RaySquareIntersection{
    bool intersectionExists;
    float t;
    float u,v;
    Vec3 intersection;
    Vec3 normal;
    Vec3 tangent;
};


class Square : public Mesh {
public:
    Vec3 m_normal;
    Vec3 m_bottom_left;
    Vec3 m_right_vector;
    Vec3 m_up_vector;

    Vec3 bottomLeft() const { return this->vertices[0].position; }
    Vec3 bottomRight() const { return this->vertices[1].position; }
    Vec3 upRight() const { return this->vertices[2].position; }
    Vec3 upLeft() const { return this->vertices[3].position; }
    Vec3 normal() const { return Vec3::cross((bottomRight() - bottomLeft()) , (upLeft() - bottomLeft())); }

    Vec3 bottomLine()const {return bottomRight() - bottomLeft();}
    Vec3 upLine() const {return upLeft() - bottomLeft();}

    Square() : Mesh() {}
    Square(Vec3 const & bottomLeft , Vec3 const & rightVector , Vec3 const & upVector , float width=1. , float height=1. ,
           float uMin = 0.f , float uMax = 1.f , float vMin = 0.f , float vMax = 1.f) : Mesh() {
        setQuad(bottomLeft, rightVector, upVector, width, height, uMin, uMax, vMin, vMax);
    }

    void setQuad( Vec3 const & bottomLeft , Vec3 const & rightVector , Vec3 const & upVector , float width=1. , float height=1. ,
                  float uMin = 0.f , float uMax = 1.f , float vMin = 0.f , float vMax = 1.f) {
        m_right_vector = rightVector;
        m_up_vector = upVector;
        m_normal = Vec3::cross(rightVector , upVector);
        m_bottom_left = bottomLeft;

        m_normal.normalize();
        m_right_vector.normalize();
        m_up_vector.normalize();

        m_right_vector = m_right_vector*width;
        m_up_vector = m_up_vector*height;

        vertices.clear();
        vertices.resize(4);
        vertices[0].position = bottomLeft;                                      vertices[0].u = uMin; vertices[0].v = vMin;
        vertices[1].position = bottomLeft + m_right_vector;                     vertices[1].u = uMax; vertices[1].v = vMin;
        vertices[2].position = bottomLeft + m_right_vector + m_up_vector;       vertices[2].u = uMax; vertices[2].v = vMax;
        vertices[3].position = bottomLeft + m_up_vector;                        vertices[3].u = uMin; vertices[3].v = vMax;
        vertices[0].normal = vertices[1].normal = vertices[2].normal = vertices[3].normal = m_normal;
        triangles.clear();
        triangles.resize(2);
        triangles[0][0] = 0;
        triangles[0][1] = 1;
        triangles[0][2] = 2;
        triangles[1][0] = 0;
        triangles[1][1] = 2;
        triangles[1][2] = 3;


    }

    Vec3 getUV(Vec3 point) const
    {
        Vec3 bottomL = bottomLine();
        Vec3 upL = upLine();

        Vec3 dir = point - bottomLeft();

        float bottomProject = Vec3::dot(dir,bottomL) / Vec3::dot(bottomL,bottomL);
        float upProject = Vec3::dot(dir,upL) / Vec3::dot(upL,upL);

        return Vec3(bottomProject,upProject,0);
    }

    bool uvCheck(Vec3 uv) const
    {
        bool ucheck = uv[0] >= 0 && uv[0] <= 1;
        bool vcheck = uv[1] >= 0 && uv[1] <= 1;

        return ucheck && vcheck;
    }

    RaySquareIntersection intersect(const Ray &ray) const {
        RaySquareIntersection intersection;

        Vec3 dir = ray.direction();// dir.normalize();
        Vec3 ori = ray.origin();

        Vec3 v1 = vertices[0].position - vertices[1].position;
        Vec3 v2 = vertices[3].position - vertices[0].position;

        Vec3 norma = normal();
        //norma.normalize();
        double ln = Vec3::dot(dir,norma);

        if(abs(ln) < 0.001) //Parrallel
        {
        	intersection.intersectionExists = false;
        }
        else
        {
            double d = Vec3::dot(bottomLeft(),norma);

    	    double t = (d - Vec3::dot(ori,norma))/ln;

            Vec3 point = ori + (dir * t); //Intersection point
            Vec3 uv = getUV(point);

            if(!uvCheck(uv)) //Point must be on plane, so after  bottom_left and not too far
            {
                intersection.intersectionExists = false;
            }
            else{
                intersection.intersectionExists = true;
                intersection.t = t;

                intersection.intersection = point;
                intersection.u = uv[0]; intersection.v = uv[1];

                intersection.normal = norma;
                intersection.tangent = v1; intersection.tangent.normalize();
            }
       }
    
        return intersection;
    }
};
#endif // SQUARE_H
