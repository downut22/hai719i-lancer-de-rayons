#ifndef MESH_H
#define MESH_H


#include <vector>
#include <string>
#include "Vec3.h"
#include "Ray.h"
#include "Triangle.h"
#include "Material.h"
#include <cstdio>
#include <cstdlib>
#include <GL/glut.h>
#include "SphereUtility.h"

#include <cfloat>

using namespace std;

// -------------------------------------------
// Basic Mesh class
// -------------------------------------------

struct MeshVertex {
    inline MeshVertex () {}
    inline MeshVertex (const Vec3 & _p, const Vec3 & _n) : position (_p), normal (_n) , u(0) , v(0) {}
    inline MeshVertex (const MeshVertex & vertex) : position (vertex.position), normal (vertex.normal) , u(vertex.u) , v(vertex.v) {}
    inline virtual ~MeshVertex () {}
    inline MeshVertex & operator = (const MeshVertex & vertex) {
        position = vertex.position;
        normal = vertex.normal;
        u = vertex.u;
        v = vertex.v;
        return (*this);
    }
    // membres :
    Vec3 position; // une position
    Vec3 normal; // une normale
    float u,v; // coordonnees uv
};

struct MeshTriangle {
    inline MeshTriangle () {
        v[0] = v[1] = v[2] = 0;
    }
    inline MeshTriangle (const MeshTriangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
    }
    inline MeshTriangle (unsigned int v0, unsigned int v1, unsigned int v2) {
        v[0] = v0;   v[1] = v1;   v[2] = v2;
    }
    unsigned int & operator [] (unsigned int iv) { return v[iv]; }
    unsigned int operator [] (unsigned int iv) const { return v[iv]; }
    inline virtual ~MeshTriangle () {}
    inline MeshTriangle & operator = (const MeshTriangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
        return (*this);
    }
    // membres :
    unsigned int v[3];
};


struct BoundingBox
{
    Vec3 min; Vec3 max;
    Vec3 center; float radius;

    Vec3 deltas(){
        return max-min;
    }

    void espilonize()
    {
        min -= Vec3(epsilon,epsilon,epsilon);
        max += Vec3(epsilon,epsilon,epsilon);
    }

    const float epsilon = 0.1;
};

class Mesh {
protected:
    void build_positions_array() {
        positions_array.resize( 3 * vertices.size() );
        for( unsigned int v = 0 ; v < vertices.size() ; ++v ) {
            positions_array[3*v + 0] = vertices[v].position[0];
            positions_array[3*v + 1] = vertices[v].position[1];
            positions_array[3*v + 2] = vertices[v].position[2];
        }
    }
    void build_normals_array() {
        normalsArray.resize( 3 * vertices.size() );
        for( unsigned int v = 0 ; v < vertices.size() ; ++v ) {
            normalsArray[3*v + 0] = vertices[v].normal[0];
            normalsArray[3*v + 1] = vertices[v].normal[1];
            normalsArray[3*v + 2] = vertices[v].normal[2];
        }
    }
    void build_UVs_array() {
        uvs_array.resize( 2 * vertices.size() );
        for( unsigned int vert = 0 ; vert < vertices.size() ; ++vert ) {
            uvs_array[2*vert + 0] = vertices[vert].u;
            uvs_array[2*vert + 1] = vertices[vert].v;
        }
    }
    void build_triangles_array() {
        triangles_array.resize( 3 * triangles.size() );
        for( unsigned int t = 0 ; t < triangles.size() ; ++t ) {
            triangles_array[3*t + 0] = triangles[t].v[0];
            triangles_array[3*t + 1] = triangles[t].v[1];
            triangles_array[3*t + 2] = triangles[t].v[2];
        }
    }    

    void computeBounds()
    {
        box.min = Vec3(FLT_MAX,FLT_MAX,FLT_MAX);
        box.max = Vec3(FLT_MIN,FLT_MIN,FLT_MIN);

        box.center = Vec3(0,0,0);

        for(int i = 0; i < vertices.size();i++)
        {
            box.min[0] = min(box.min[0],vertices[i].position[0]);
            box.min[1] = min(box.min[1],vertices[i].position[1]);
            box.min[2] = min(box.min[2],vertices[i].position[2]);

            box.max[0] = max(box.max[0],vertices[i].position[0]);
            box.max[1] = max(box.max[1],vertices[i].position[1]);
            box.max[2] = max(box.max[2],vertices[i].position[2]);

            box.center += vertices[i].position;
        }

        box.center /= (float)(vertices.size());
        box.radius = 0;

        for(int i = 0; i < vertices.size();i++)
        {
            float d = (vertices[i].position - box.center).length();
            if(d > box.radius){box.radius = d;}
        }

        box.espilonize();
    }
public:
    BoundingBox box;

    std::vector<MeshVertex> vertices;
    std::vector<MeshTriangle> triangles;
    std::vector<Vec3> normals;

    std::vector< float > positions_array;
    std::vector< float > normalsArray;
    std::vector< float > uvs_array;
    std::vector< unsigned int > triangles_array;

    Material material;

    void loadOFF (const std::string & filename);
    void recomputeNormals ();
    void centerAndScaleToUnit ();
    void scaleUnit ();


    virtual
    void build_arrays() {
        recomputeNormals();
        build_positions_array();
        build_normals_array();
        build_UVs_array();
        build_triangles_array();
    }

    Mesh(){}
    //Mesh(Mesh const &);
    //Mesh & operator = (Mesh const &);
    Mesh(std::string const & filename,Vec3 position,Vec3 rotation, Vec3 size)
    {
        openMesh(filename,vertices,triangles,false);

        scale(size);
        rotate_x(rotation[0]);rotate_y(rotation[1]);rotate_z(rotation[2]);
        translate(position);

        computeBounds();

        build_arrays();
    }

void openMesh( std::string const & filename,
              std::vector<MeshVertex> & o_vertices,
              std::vector< MeshTriangle > & o_triangles,bool readNormals)
{
    std::ifstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open())
    {
        std::cout << filename << " cannot be opened" << std::endl;
        return;
    }

    std::string magic_s;

    myfile >> magic_s;

    if( magic_s != "OFF" )
    {
        std::cout << magic_s << " != OFF :   We handle ONLY *.off files." << std::endl;
        myfile.close();
        exit(1);
    }

    int n_vertices , n_faces , dummy_int;
    myfile >> n_vertices >> n_faces >> dummy_int;

    o_vertices.clear();

    for( int v = 0 ; v < n_vertices ; ++v )
    {
        float x , y , z , a , b , c ;

        myfile >> x >> y >> z ;
	if(readNormals)
        	myfile >> a >> b >> c ;

        o_vertices.push_back( MeshVertex( Vec3( x , y , z ) , Vec3(a,b,c) ));
    }

    o_triangles.clear();
    for( int f = 0 ; f < n_faces ; ++f )
    {
        int n_vertices_on_face;
        myfile >> n_vertices_on_face;

        if( n_vertices_on_face == 3 )
        {
            unsigned int _v1 , _v2 , _v3;
            myfile >> _v1 >> _v2 >> _v3;

            o_triangles.push_back(MeshTriangle( _v1, _v2, _v3 ));
        }
        else if( n_vertices_on_face == 4 )
        {
            unsigned int _v1 , _v2 , _v3 , _v4;
            myfile >> _v1 >> _v2 >> _v3 >> _v4;

            o_triangles.push_back(MeshTriangle(_v1, _v2, _v3 ));
            o_triangles.push_back(MeshTriangle(_v1, _v3, _v4));
        }
        else
        {
            std::cout << "We handle ONLY *.off files with 3 or 4 vertices per face not " << n_vertices_on_face << std::endl;
            myfile.close();
            exit(1);
        }
    }

}

    void translate( Vec3 const & translation ){
        for( unsigned int v = 0 ; v < vertices.size() ; ++v ) {
            vertices[v].position += translation;
        }
    }

    void apply_transformation_matrix( Mat3 transform ){
        for( unsigned int v = 0 ; v < vertices.size() ; ++v ) {
            vertices[v].position = transform*vertices[v].position;
        }

        //        recomputeNormals();
        //        build_positions_array();
        //        build_normals_array();
    }

    void scale( Vec3 const & scale ){
        Mat3 scale_matrix(scale[0], 0., 0.,
                0., scale[1], 0.,
                0., 0., scale[2]); //Matrice de transformation de mise à l'échelle
        apply_transformation_matrix( scale_matrix );
    }

    void rotate_x ( float angle ){
        float x_angle = angle * M_PI / 180.;
        Mat3 x_rotation(1., 0., 0.,
                        0., cos(x_angle), -sin(x_angle),
                        0., sin(x_angle), cos(x_angle));
        apply_transformation_matrix( x_rotation );
    }

    void rotate_y ( float angle ){
        float y_angle = angle * M_PI / 180.;
        Mat3 y_rotation(cos(y_angle), 0., sin(y_angle),
                        0., 1., 0.,
                        -sin(y_angle), 0., cos(y_angle));
        apply_transformation_matrix( y_rotation );
    }

    void rotate_z ( float angle ){
        float z_angle = angle * M_PI / 180.;
        Mat3 z_rotation(cos(z_angle), -sin(z_angle), 0.,
                        sin(z_angle), cos(z_angle), 0.,
                        0., 0., 1.);
        apply_transformation_matrix( z_rotation );
    }


    void draw() const {
        if( triangles_array.size() == 0 ) return;
        GLfloat material_color[4] = {material.diffuse_material[0],
                                     material.diffuse_material[1],
                                     material.diffuse_material[2],
                                     1.0};

        GLfloat material_specular[4] = {material.specular_material[0],
                                        material.specular_material[1],
                                        material.specular_material[2],
                                        1.0};

        GLfloat material_ambient[4] = {material.ambient_material[0],
                                       material.ambient_material[1],
                                       material.ambient_material[2],
                                       1.0};

        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, material_specular);
        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material_color);
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, material_ambient);
        glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, material.shininess[0]);

        glEnableClientState(GL_VERTEX_ARRAY) ;
        glEnableClientState (GL_NORMAL_ARRAY);
        glNormalPointer (GL_FLOAT, 3*sizeof (float), (GLvoid*)(normalsArray.data()));
        glVertexPointer (3, GL_FLOAT, 3*sizeof (float) , (GLvoid*)(positions_array.data()));
        glDrawElements(GL_TRIANGLES, triangles_array.size(), GL_UNSIGNED_INT, (GLvoid*)(triangles_array.data()));

    }

	RayTriangleIntersection intersectTriangle(Ray const & ray, Vec3* points) const
	{
		RayTriangleIntersection rti;
		rti.intersectionExists = false;

		 Vec3 dir = ray.direction();// dir.normalize();
        Vec3 ori = ray.origin();

        Vec3 v1 = points[1] - points[0];
        Vec3 v2 = points[2] - points[0];

        Vec3 norma = Vec3::cross(v1,v2); //norma.normalize();

        double ln = Vec3::dot(dir,norma);

        if(abs(ln) > 0.001)
        {
            double d = Vec3::dot(points[0],norma);

    	    double t = (d - Vec3::dot(ori,norma))/ln;
            if(t<0){return rti;}

            Vec3 point = ori + (dir * t); //Intersection point
            Vec3 uv;

            Vec3 edge1 = points[1] - points[0];
            Vec3 v1 = point - points[0];
            Vec3 C1 = Vec3::cross(edge1,v1);
            uv[0] = Vec3::dot(norma, C1);
            bool e1 = uv[0] >= -0.01;

            Vec3 edge2 = points[2] - points[1];
            Vec3 v2 = point - points[1];
            Vec3 C2 = Vec3::cross(edge2,v2);
            uv[1] = Vec3::dot(norma, C2);
            bool e2 = uv[1] >= -0.01;

            Vec3 edge3 = points[0] - points[2];
            Vec3 v3 = point - points[2];
            Vec3 C3 = Vec3::cross(edge3,v3);
            uv[2] = Vec3::dot(norma, C3);
            bool e3 = uv[2] >= -0.01;

            //if(ucheck && vcheck && uvcheck){
            if(e1 && e2 && e3){
                rti.intersectionExists = true;
                rti.t = t;
                rti.intersection = point;
                rti.normal = norma; rti.normal.normalize();
                rti.tangent = v1; rti.tangent.normalize();
                rti.uvs = uv;
            }
        }
	
		return rti;
	}

    RayTriangleIntersection intersect( Ray const & ray ) const 
    {
        RayTriangleIntersection closestIntersection;

        closestIntersection.t = FLT_MAX;
        closestIntersection.intersectionExists = false;

        RaySphereIntersection rsi = sphereIntersect(ray,box.center,box.radius);

	//Check if we're inside the bounding sphere 
        if(rsi.intersectionExists)
        {
            for(int i = 0; i < triangles.size();i++)
            {
                Vec3 points[3];
                points[0] = vertices[triangles[i].v[0]].position;
                points[1] = vertices[triangles[i].v[1]].position;
                points[2] = vertices[triangles[i].v[2]].position;

                RayTriangleIntersection rti = intersectTriangle(ray,points);

                if(rti.intersectionExists && rti.t < closestIntersection.t)
                {
                    closestIntersection = rti;
                }
            }
        }

        // Note :
        // Creer un objet Triangle pour chaque face
        // Vous constaterez des problemes de précision
        // solution : ajouter un facteur d'échelle lors de la création du Triangle : float triangleScaling = 1.000001;
    
        return closestIntersection;
    }
};




#endif
