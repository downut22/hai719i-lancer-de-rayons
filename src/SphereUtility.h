#ifndef SphereU_H
#define SphereU_H

struct RaySphereIntersection{
    bool intersectionExists;
    float t;
    float theta,phi;
    float u,v;
    Vec3 intersection;
    Vec3 secondintersection;
    Vec3 normal;
    Vec3 tangent;
};

static
Vec3 SphericalCoordinatesToEuclidean( Vec3 ThetaPhiR ) {
    return ThetaPhiR[2] * Vec3( cos(ThetaPhiR[0]) * cos(ThetaPhiR[1]) , sin(ThetaPhiR[0]) * cos(ThetaPhiR[1]) , sin(ThetaPhiR[1]) );
}
static
Vec3 SphericalCoordinatesToEuclidean( float theta , float phi ) {
    return Vec3( cos(theta) * cos(phi) , sin(theta) * cos(phi) , sin(phi) );
}

static
Vec3 EuclideanCoordinatesToSpherical( Vec3 xyz ) {
    float R = xyz.length();
    float phi = asin( xyz[2] / R );
    float theta = atan2( xyz[1] , xyz[0] );
    return Vec3( theta , phi , R );
}


  static RaySphereIntersection sphereIntersect(const Ray &ray, Vec3 center,float radius)  {
        RaySphereIntersection intersection;

        //TODO calcul l'intersection rayon sphere
        Vec3 dir = ray.direction();
        Vec3 ori = ray.origin();

        float a = (dir[0] * dir[0]) + (dir[1] * dir[1]) + (dir[2] * dir[2]);

        //float b = 2 * (dir[0] * (ori[0] - m_center[0]) + dir[1] * (ori[1] - m_center[1]) + dir[2] * (ori[2] - m_center[2])) //2 * ((dir[0] * ori[0]) + (dir[1] * ori[1]) + (dir[2] * ori[2]));
        Vec3 oc = (ori - center);
        float b = 2 * ((dir[0] * oc[0]) + (dir[1] * oc[1]) + (dir[2] * oc[2]));

        float c = (oc.length() * oc.length()) - (radius * radius); //(dir[0] * (ori[0] - m_center[0]) + dir[1] * (ori[1] - m_center[1]) + dir[2] * (ori[2] - m_center[2]))

        float delta = (b*b) - (4*a*c);

        if(delta < 0)
        {
            intersection.intersectionExists = false;
        }
        else
        {
            float t1 = ((-b) - sqrt(delta)) / (2*a);
            float t2 = ((-b) + sqrt(delta)) / (2*a);

            //if(t2 < t1){float temp = t1; t1 = t2; t2 = temp;}

            intersection.intersectionExists = true;
            intersection.t = t1;

            intersection.intersection = ray.origin() + (ray.direction() * t1);
            intersection.secondintersection = ray.origin() + (ray.direction() * t2);

            Vec3 point = intersection.intersection - center;

            Vec3 sp = EuclideanCoordinatesToSpherical(point);
            intersection.theta = sp[0]; intersection.u = 0.5f + (sp[0] / (2*M_PI));
            intersection.phi = sp[1]; intersection.v = 0.5f + (sp[1] / (2*M_PI));


            intersection.normal = (intersection.intersection - center);
            intersection.normal.normalize();

            intersection.tangent = intersection.normal; //<<<<<<<<<<<<<<< TO CHANGE
        }

        return intersection;
    }

#endif

