
#define COLOR_H
#include "Vec3.h"
#include <cstdlib>

struct Color
{
public:
    unsigned char R,G,B;
    int H;float S,L;

    Color(Vec3 rgb)
    {
        R = (int)(rgb[0]*255); G = (int)(rgb[1]*255); B = (int)(rgb[2]*255);
    }
    Color(float _r, float _g, float _b, float _h, float _s, float _l)
    {
        R = (int)(_r*255); G = (int)(_g*255); B = (int)(_b*255);
        H = (int)(_h*360); S = _s; L = _l;
    }

    void shift(int angle)
    {
        H = (H+angle) % 360;
    }
    void saturate(float coef)
    {
        S = std::max(0.0f,std::min(1.0f,S*coef));
    }
    void enlight(float coef)
    {
        L = std::max(0.0f,std::min(1.0f,L*coef));
    }

    Vec3 getRGB()
    {
        return Vec3(R/255.0,G/255.0,B/255.0);
    }

    float HtoRGB(float v1, float v2, float vH)
    {
        if(vH < 0){
            vH += 1;
        }
        if(vH > 1){
            vH -= 1;
        }

        if((6 * vH) < 1)
        {
            return (v1 + (v2 - v1) * 6 * vH);
        }
        else if((2 * vH) < 1)
        {
            return v2;
        }
        else if((3*vH) < 2)
        {
            return (v1 + (v2 - v1) * ((2.0f / 3.0f) - vH) * 6);
        }
        else{
            return v1;
        }
    }

    void computeRGB()
    {
        if(S == 0)
        {
            R = G = B = L * 255;
        }
        else
        {
            float v1,v2;
            float h = H / 360.0f;

            v2 =(L < 0.5) ? (L * (1+S)) : ((L + S) - (L * S));
            v1 = (2 * L) - v2;

            R = (unsigned char)(255 * HtoRGB(v1,v2,h + (1.0f / 3.0f)));
            G = (unsigned char)(255 * HtoRGB(v1,v2,h));
            B = (unsigned char)(255 * HtoRGB(v1,v2,h - (1.0f / 3.0f)));
        }

        /*
        unsigned char region, remainder, p, q, t;
    
        if (s == 0)
        {
            r = v;
            g = v;
            b = v;
            return;
        }
    
        region = h / 43;
        remainder = (h - (region * 43)) * 6; 
    
        p = (v * (255 - s)) >> 8;
        q = (v * (255 - ((s * remainder) >> 8))) >> 8;
        t = (v * (255 - ((s * (255 - remainder)) >> 8))) >> 8;
    
        switch (region)
        {
            case 0:
                r = v; g = t; b = p;
                break;
            case 1:
                r = q; g = v; b = p;
                break;
            case 2:
                r = p; g = v; b = t;
                break;
            case 3:
                r = p; g = q; b = v;
                break;
            case 4:
                r = t; g = p; b = v;
                break;
            default:
                r = v; g = p; b = q;
                break;
        }*/
    }

    void computeHSL()
    {
        float r = R / 255.0;
        float b = B / 255.0;
        float g = G / 255.0;

        float min =  r < g ? (r < b ? r : b) : (g < b ? g : b);
        float max =  r > g ? (r > b ? r : b) : (g > b ? g : b);
        float delta = max-min;

        L = (max + min) / 2.0f;

        if(delta == 0)
        {
            H = 0; S = 0.0f;
        }
        else
        {
            S = (L <= 0.5f) ? (delta / (max+min)) : (delta / (2.0f - max - min));

            float h;
            if(r == max)
            {
                h = ((g-b) / 6.0f) / delta;
            }
            else if(g == max)
            {
                h = (1.0f / 3.0f) + ((b - r) / 6.0f) / delta;
            }
            else
            {
                h = (2.0f / 3.0f) + ((r - g) / 6.0f) / delta;
            }

            if(h < 0) { h += 1;}
            else if(h > 1){h -= 1;}

            H = (int)(h * 360);
        }

        /*unsigned char rgbMin, rgbMax;

        rgbMin = r < g ? (r < b ? r : b) : (g < b ? g : b);
        rgbMax = r > g ? (r > b ? r : b) : (g > b ? g : b);
    
        v = rgbMax;
        if (v == 0)
        {
            h = 0;
            s = 0;
            return;
        }

        s = 255 * long(rgbMax - rgbMin) / v;
        if (s == 0)
        {
            h = 0;
            return;
        }

        if (rgbMax == r)
            h = 0 + 43 * (g - b) / (rgbMax - rgbMin);
        else if (rgbMax == g)
            h = 85 + 43 * (b - r) / (rgbMax - rgbMin);
        else
            h = 171 + 43 * (r - g) / (rgbMax - rgbMin);
        */

    }
};
