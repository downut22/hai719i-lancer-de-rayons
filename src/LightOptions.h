
enum LightMode{
    Additive,
    Led
};


struct LightOptions
{
    int sampleCount = 1;

    int bounceCount = 3;
    float intersectionDelta = 0.001;

    Vec3 scene_color = Vec3(0,0,0);
    float ambient_light = 0.03;
    float global_light_coef = 2.0;

    float lightIntensityCurve = 0.5;
    int shadowSoftness = 10;
    float reflectionCurve = 1;

    //int prismeCount = 10;

    LightMode lightMode = Led;

    //----Additive Mode
    float lightsContribution = 0.3;

    //----Postprocessing
    int hueShift = 0;
    float luminosity = 1.55;
    float saturation = 1.3;

    int maxBlurSize = 6;
    float focalDistance = 8;
    float focalRange = 2;

    LightOptions(){}
};
