#ifndef MATERIAL_H
#define MATERIAL_H

#include "imageLoader.h"
#include "stb_image.h"
#include "Vec3.h"
#include <cmath>

#include <GL/glut.h>

enum MaterialType {
    Material_Diffuse_Blinn_Phong ,
    Material_Glass,
    Material_Mirror,
    Material_Metal
};

struct Texture
{
    Vec3* data; int size;
    int width,height,nbChannels;
    bool enabled;

    Vec3 get(double u, double v)
    {
        return get((int)(u * (width-1)), (int)(v * (height-1)));
    }
    Vec3 get(int x, int y)
    {
        if(x < 0 || x >= width){std::cout<<"Width out of bounds ! " << x << std::endl; return Vec3(0,0,0);}
        if(y < 0 || y >= height){std::cout<<"Height out of bounds ! " << y << std::endl;return Vec3(0,0,0);}
        return data[x + (y * width)];
    }

    Texture(char* path)
    {
        unsigned char* dataChar = stbi_load(path, &width, &height, &nbChannels, 0);
        if (!dataChar) {
            enabled = false;
            std::cout << "Failed to load image at path " << path << std::endl;
        }
        else
        {
            size = width*height;
            data = new Vec3[size];

            int chari = 0;
            for(int i = 0 ; i < size;i++)
            {
                data[i] = Vec3(
                    dataChar[chari] / 255.0,
                    (nbChannels >= 2) ? (dataChar[chari+1] / 255.0) : 0,
                    (nbChannels >= 3) ? (dataChar[chari+2] / 255.0) : 0);
                chari += nbChannels;
            }

            enabled = true;

            std::cout<<"Loaded texture <" << path << "> : width is " << width << " ; height is " << height << " ; nbChannels is " << nbChannels << std::endl;
        }
        stbi_image_free(dataChar);
    }
    Texture(){enabled = false;}
};


struct Material
{
    Vec3 ambient_material;
    Vec3 diffuse_material;
    Vec3 specular_material;

    Vec3 shininess; //Specular x : min , y : max , z : curve
    float metalness;
    float roughness;

    //===========================================

    float index_medium;
    float light_medium;
    float light_medium_pow;

    float transparency;
    float prismarity;

    //===========================================

    MaterialType type;
    Vec3 mirror_material;

    //===========================================

    Texture colorTexture;
    Texture normalTexture;
    Texture metalTexture;
    Texture roughTexture;

    void loadColorTexture(char* path){colorTexture = Texture(path);}
    void loadNormalTexture(char* path){normalTexture = Texture(path);}
    void loadMetalTexture(char* path){metalTexture = Texture(path);}
    void loadRoughTexture(char* path){roughTexture = Texture(path);}

    //===========================================

    Vec3 getSpecular(double u, double v)
    {
        return specular_material * (colorTexture.enabled ? colorTexture.get(u,v) : Vec3(1,1,1));
    }
    Vec3 getDiffuse(double u, double v)
    {
        return diffuse_material * (colorTexture.enabled ? colorTexture.get(u,v) : Vec3(1,1,1));
    }

    Vec3 getNormal(double u, double v)
    {
        return normalTexture.enabled ? ((normalTexture.get(u,v) * 2) - Vec3(1,1,1)) : Vec3(0,0,1);
    }
    double getMetal(double u, double v)
    {
        return metalness * (metalTexture.enabled ? metalTexture.get(u,v)[0] : 1);
    }
    double getRough(double u, double v)
    {
        return roughness * (roughTexture.enabled ? roughTexture.get(u,v)[0] : 1);
    }

    //===========================================

    bool nul;

    Material() {
        type = Material_Diffuse_Blinn_Phong;
        transparency = 0.0;
        index_medium = 1.0;
        ambient_material = Vec3(0., 0., 0.);
        roughness = 0;
        metalness = 0;
        nul = false;
    }
};



#endif // MATERIAL_H
